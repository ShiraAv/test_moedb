<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "bonus".
 *
 * @property integer $id
 * @property integer $userId
 * @property integer $reasonId
 * @property integer $amount
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $user
 * @property Bonusreason $reason
 */
class Bonus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bonus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'reasonId'], 'required'],
            [['userId', 'reasonId', 'amount', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
			['amount', 'compare', 'compareValue' => 100, 'operator' => '>='],
			['amount', 'compare', 'compareValue' => 1000, 'operator' => '<='],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['userId' => 'id']],
            [['reasonId'], 'exist', 'skipOnError' => true, 'targetClass' => Bonusreason::className(), 'targetAttribute' => ['reasonId' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
	     /**
     * Defenition of relation to user table
     */    

	 public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => 'yii\behaviors\TimestampBehavior',
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'reasonId' => 'Reason ID',
            'amount' => 'Amount',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReason()
    {
        return $this->hasOne(Bonusreason::className(), ['id' => 'reasonId']);
    }
	//question 3b
		public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }	
	//question 3b
	public function getUpdateddBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
