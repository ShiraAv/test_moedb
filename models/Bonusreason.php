<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "bonusreason".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Bonus[] $bonuses
 */
class Bonusreason extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bonusreason';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBonuses()
    {
        return $this->hasMany(Bonus::className(), ['reasonId' => 'id']);
    }
	
	////Add to dropDown
	public static function getBonus()
	{
		$allBonus = self::find()->all();
		$allBonusArray = ArrayHelper::
					map($allBonus, 'id', 'name');
		return $allBonusArray;						
	}
	////Add to dropDown
	public function getBonusItem()
	{
      return $this->hasOne(Bonus::className(), ['id' => 'reasonId']);
	}
}
